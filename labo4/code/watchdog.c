
/**
* \file watchdog.c
* \brief Fonctions permettant la mise en place d'un watchdog.
* \author Y. Thoma & D. Molla
* \version 0.1
* \date 08 novembre 2019
*
* Le watchdog proposé est implémenté à l'aide de 2 tâches responsables
* de détecter une surcharge du processeur. Si tel est le cas, une fonction
* fournie par le développeur est appelée. Elle devrait suspendre ou détruire
* les tâches courantes, afin de redonner la main à l'OS.
*
* Code Xenomai 3.0
*
*/

#include <alchemy/task.h>
#include <alchemy/alarm.h>
#include <alchemy/timer.h>

#include "watchdog.h"
#include "general.h"


static void (*watchdog_suspend_function)(void) = NULL; /**< Fonction appelée lors d'une surcharge */



int start_watchdog(void(* suspend_func)(void))
{
  watchdog_suspend_function = suspend_func;

  return 1;
}


int end_watchdog(void)
{

  return 1;
}
