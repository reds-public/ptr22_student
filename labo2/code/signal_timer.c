#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>

struct timespec previous;
int nbMeasures;
volatile sig_atomic_t loop = 1;

void signal_handler (int signum)
{
    struct timespec current;
    static int i = 1;
    long nbSec;
    long result;

    /* Get the current time */
    clock_gettime(CLOCK_REALTIME, &current);

    /* Get the difference in seconds */
    nbSec = current.tv_sec - previous.tv_sec;

    /* Get the difference in nseconds */
    result = (1000000000 * nbSec) - previous.tv_nsec + current.tv_nsec;

    /* Set the previous to match the current */
    previous = current;

    /* Print the result */
    fprintf(stdout,
            "%ld.%09ld\n",
            result / 1000000000, result % 1000000000);

    /* Check the measures */
    if (i < nbMeasures) {
        ++i;
    } else {
	    fprintf(stdout, "loop 0\n");
        loop = 0;
    }
}

int main (int argc, char *argv[])
{
    long usec;
    long nsec;

    struct sigevent event;
    struct itimerspec spec;

    timer_t timer;

    /* Test the parameters */
    if (argc != 3) {
        fprintf(stderr, "Wrong parameter(s)\n");
        return EXIT_FAILURE;
    }

    /* Set the measures and duration */
    nbMeasures = strtoimax(argv[1], (char **)NULL, 10);
    usec = (long)strtol(argv[2], (char **)NULL, 10);
    if (nbMeasures <= 0 || usec <= 0) {
        fprintf(stderr,
                "Both the number of measures and the time (in us) must be > 0\n");
        return EXIT_FAILURE;
    }

    /*
     * Set the signal handler. SIGRTMIN = lowest-indexed POSIX real-time signal
     */
    signal(SIGRTMIN, signal_handler);

    /* Set up the event and timer spec */
    event.sigev_notify = SIGEV_SIGNAL;
    event.sigev_signo = SIGRTMIN;

    nsec = usec * 1000;

    spec.it_interval.tv_sec = nsec / 1000000000;
    spec.it_interval.tv_nsec = nsec % 1000000000;
    spec.it_value = spec.it_interval;

    /* Set the initial time */
    clock_gettime(CLOCK_REALTIME, &previous);

    /* Create the timer */
    if (timer_create(CLOCK_REALTIME, &event, &timer)) {
        fprintf(stderr, "Cannot create timer\n");
        return EXIT_FAILURE;
    }

    /* Start the timer */
    if (timer_settime(timer, 0, &spec, NULL)) {
        fprintf(stderr, "Cannot start timer\n");
        return EXIT_FAILURE;
    }

    /* Loop until the measures are done */
    while(loop);

    return EXIT_SUCCESS;
}
