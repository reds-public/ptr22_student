#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <math.h>
#include <complex.h>

#include <cobalt/stdio.h>

#include <alchemy/task.h>
#include <alchemy/timer.h>
#include <alchemy/event.h>
#include <alchemy/queue.h>

#include "board.h"

/* NOTE : Si nécessaire, vous pouvez bien sûr modifier la période des tâches */
#define ACQUISITION_TASK_PERIOD         ((RTIME)1000000UL)
static RT_TASK acquisition_rt_task;
#define PROCESSING_TASK_PERIOD          ((RTIME)1000000UL)
static RT_TASK processing_rt_task;
#define LOG_TASK_PERIOD                 ((RTIME)1000000UL)
static RT_TASK log_rt_task;

#define FFT_BINS                        (8192)  // Number of samples for the FFT

typedef double complex cplx;

/* SOURCE : https://rosettacode.org/wiki/Fast_Fourier_transform#C */
void _fft(cplx buf[], cplx out[], int n, int step)
{
	if (step < n) {
		_fft(out, buf, n, step * 2);
		_fft(out + step, buf + step, n, step * 2);

		for (int i = 0; i < n; i += 2 * step) {
			cplx t = cexpf(-I * (M_PI * i) / n) * out[i + step];
			buf[i / 2]     = out[i] + t;
			buf[(i + n)/2] = out[i] - t;
		}
	}
}

void fft(cplx buf[], cplx out[], int n)
{
	for (int i = 0; i < n; i++) {
    out[i] = buf[i];
  }

	_fft(buf, out, n, 1);
}

void log_task(void *cookie)
{
    /* TODO : afficher à la console la fréquence ayant la puissance maximale
     * et le temps nécessaire pour calculer cette valeur.
     * Exemple d'affichage : Dominant frequency : 64Hz, computation time : 800ms */
}

void acquisition_task(void *cookie)
{
    /* TODO : reprendre et adapter le code d'exemple ci-dessous pour
     * effectuer une acquisition d'un certain nombre d'échantillons puis les
     * envoyer à la tâche *processing_task*.
     *
     * Vous allez effectuer un échantillonage d'un nombre d'éléments égal à
     * FFT_BINS. C'est sur cette ensemble entier que la tâche processing doit
     * appliquer son traitement.
     */
    data_t *samples, *samples_p;
    const size_t nb_samples = 1024;
    samples = malloc(sizeof(data_t) * 2 * nb_samples);

    rt_task_set_periodic(&acquisition_rt_task, TM_NOW, ACQUISITION_TASK_PERIOD);

    /* EXAMPLE : */
    while (1) {
        size_t to_read = sizeof(data_t) * 2 * nb_samples;
        samples_p = samples;
        while (to_read != 0) {
            rt_task_wait_period(NULL);
            ssize_t read = read_samples(samples_p, to_read);
            if (read < 0) {
                rt_printf("Error while reading samples\n");
                continue;
            }
            // Attention, la valeur retournée par read_samples
            // est en octet
            samples_p += (void*)read;
            to_read -= read;
        }
    }
}

void processing_task(void *cookie)
{

    /* TODO : compléter la tâche processing de telle sorte qu'elle recoive les
     * échantillons de la tâche acquisition. Une fois reçu les échantillons,
     * appliquer une FFT à l'aide de la fonction fft fournie, puis trouver
     * la fréquence à laquelle la puissance est maximale.
     * Enfin, envoyer à la tâche *log* le résultat, ainsi
     * que le temps qui a été nécessaire pour effectuer le calcul.
     *
     * Notez bien que le codec audio échantillone à 48 Khz.
     * Notez aussi que le driver audio renvoie des échantillons stéréo interlacés.
     * Vous n'effectuerez une FFT que sur un seul canal. En conséquence, prenez un
     * échantillon sur deux. */
    cplx *out = malloc(sizeof(cplx) * FFT_BINS); // Auxiliary array for fft function
    data_t *x; // NOTE : discrete time signal sent by the acquisition task
    cplx *buf = malloc(sizeof(cplx) * FFT_BINS);
    double *power = malloc(sizeof(double) * FFT_BINS);

    /* EXAMPLE using the fft function : */
    for (size_t i = 0; i < FFT_BINS; i++) {
        buf[i] = x[i*2] + 0*I; // Only take left channel !!!
    }
    fft(buf, out, FFT_BINS);
    for (size_t i = 0; i < FFT_BINS; i++) {
        double re = crealf(buf[i]);
        double im = cimagf(buf[i]);
        power[i] = re * re + im * im;

        /* DO SOMETHING with those values */
    }
}

int
main(int argc, char *argv[])
{
    if (argc < 1) {
        printf("Not enough arguments. \
		Expected %s \n", argv[0]);
        return EXIT_SUCCESS;
    }

    if (init_board() != 0) {
        perror("Error at board initialization.");
        exit(EXIT_FAILURE);
    }

    printf("Init board successfull\n");

    mlockall(MCL_CURRENT | MCL_FUTURE);

    if (rt_task_spawn(&acquisition_rt_task, "acquisition task", 0, 50, T_JOINABLE, acquisition_task, NULL) != 0) {
        perror("Error while starting acquisition_task");
        exit(EXIT_FAILURE);
    };
    printf("Launched acquisition task\n");
    if (rt_task_spawn(&processing_rt_task, "processing task", 0, 50, T_JOINABLE, processing_task, NULL) != 0) {
        perror("Error while starting processing_task");
        exit(EXIT_FAILURE);
    };
    printf("Launched processing task\n");
    if (rt_task_spawn(&log_rt_task, "log task", 0, 50, T_JOINABLE, log_task, NULL) != 0) {
        perror("Error while starting log task");
        exit(EXIT_FAILURE);
    };
    printf("Launched log task\n");
    rt_task_join(&acquisition_rt_task);
    rt_task_join(&processing_rt_task);
    rt_task_join(&log_rt_task);

    clean_board();
    munlockall();

    return EXIT_SUCCESS;
}
